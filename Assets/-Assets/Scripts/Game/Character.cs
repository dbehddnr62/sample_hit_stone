﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("케릭터 타격 리소스")]
    public List<Sprite> mSprites; //케릭터 타격 리소스 넣는곳
    public SpriteRenderer mCharacter_Renderer;

    [Header("Effect")]
    public C_Effect mEffect;

    //케릭터 타격 플레이 
    public void Attack_Play()
    {
        mEffect.Effect_Play(); //Effect animation play

        mAttack_Animation_Coroutine = StartCoroutine(@Attack_Animation_Coroutine()); //coroutine play
    }

    //타격 코루틴 변수 선언
    Coroutine mAttack_Animation_Coroutine = null;
    IEnumerator @Attack_Animation_Coroutine()
    {
        for (int i = 0; i < mSprites.Count; i++) //이미지수 만큼 실행
        {
            mCharacter_Renderer.sprite = mSprites[i]; //이미지 변경
            yield return new WaitForSeconds(GlobalValue.mCharacter_Animation_Speed); //0.1초 대기
        }
    }
}
