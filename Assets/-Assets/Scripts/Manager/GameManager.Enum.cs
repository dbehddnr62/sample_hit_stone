﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour  //Enum
{
    //게임에 전체 메뉴 레이어를 타입을 정의 한다.
    public enum MENUTYPE { 
        MAINMENU, //0
        GAMEMENU, //1
        ENDMENU,
    }


    public enum GAMETYPE
    {
        NONE,
        SECONDS, //100초 동안 얼마나 많은 스톤을 깨지는지.
        STONE,   //100개의 스톤을 얼마나 빨리 깨는지.
        MEMORY,  //기억한다음 스톤을 친다.
    }

    public enum GAMEMEMORY_STEP { 
        NONE,
        DRAW,  //블록을 그린다.
        HIDE,  //블록을 가린다.
        CHECK, //블록이 다 사라졌는지
    
    }
}
