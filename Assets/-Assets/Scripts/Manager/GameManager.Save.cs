﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour //Save
{
    public void SaveDataLoad()
    {
        mBastScore = getBastScore();
        mBastGameTime = getBastTime();

        //베스트 스코어가 없을경우 0 나온다. 그래서 GlobalValue.mGameMaxTime 넣어준다.
        if (mBastGameTime == 0)
        {
            mBastGameTime = GlobalValue.mGameMaxTime;
        }
    }

    public void setBastScore(int _value)
    {
        mBastScore = _value;
        PlayerPrefs.SetInt(GlobalString.mSaveBastScore_str, _value);
    }

    public int getBastScore()
    {
        mBastScore = PlayerPrefs.GetInt(GlobalString.mSaveBastScore_str);
        return mBastScore;
    }

    public void setBastTime(float _value)
    {
        mBastGameTime = _value;
        PlayerPrefs.SetFloat(GlobalString.mSaveBastTime_str, _value);
    }

    public float getBastTime()
    {
        mBastGameTime = PlayerPrefs.GetFloat(GlobalString.mSaveBastTime_str);
        return mBastGameTime;
    }

}
