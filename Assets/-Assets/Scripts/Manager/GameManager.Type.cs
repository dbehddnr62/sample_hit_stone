﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour //Type 게임 종류를 정의 한다.
{
    public GAMETYPE mGAMETYPE = GAMETYPE.NONE;

    public void OnSecondsGameType() //Button 인터페이스로 연결
    {
        setGameType(GAMETYPE.SECONDS);
    }

    public void OnStoneGameType() //Button 인터페이스로 연결
    {
        setGameType(GAMETYPE.STONE);
    }

    public void OnMemoryGameType() //버튼과 연결
    {
        setGameType(GAMETYPE.MEMORY);
    }


    public void setGameType(GAMETYPE _GameType)
    {
        Debug.Log("setGameType : " + _GameType.ToString());

        //게임과 관련된 초기화 코드는 여기에 넣는다.
        switch(_GameType)
        {
            case GAMETYPE.NONE:
                break;
            case GAMETYPE.SECONDS:
                break;
            case GAMETYPE.STONE:
                break;
            case GAMETYPE.MEMORY:
                Init_GameMemory();
                break;

        }

        mGAMETYPE = _GameType;


    }
}
