﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalValue
{
    //블록 총 게수
    public static int mBloclMax = 20;

    //케릭터 에니메이션 속도
    public static float mCharacter_Animation_Speed = 0.1f;

    //케릭터 Effect 에니메이션 속도
    public static float mCharacter_Effect_Animation_Speed = 0.05f;

    //100초 타임 게임 설정 값
    public static float mGameMaxTime = 100f;

    //기억력 게임 총 회수
    public static int mGameMemoryMaxCount = 9;

    //팝업을 사용했는지?
    public static bool mPopupEnable = false;
}
